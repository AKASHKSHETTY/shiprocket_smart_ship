# API doc

## Dashboard Route

`/admin_home/storeName`

It is a home URL. Here we need to show the dashboard.

## API for total orders page

GET `/home/:store`

Here we can see the total orders.

## API for list of orders

GET `/DHLOrders/:store`

Here we can see the list of orders.

## API for settings data

GET `/settings/:store`

Here we can check the settings data

