const Session =require('../model/session');
const request = require('request-promise');
const dotenv = require("dotenv").config();
var vas_price_value=0;

let carrierApi=async(req,res)=>{
    console.log("incheckout");
    return   res.json({"rates":[{"service_name":"Jumpstart DEMO","service_code":"DOFF","total_price":400,"description":"Demo Address","currency":"INR" }]})
    const response =req.body;
   
    let line_items=response.rate.items;
   console.log(req.body,line_items);
    try {
        //fetch eunumart id
        for (var i = 0; i <line_items.length ; i++) {
            if(line_items[i].properties!==null){
                var token=line_items[i].properties._eunimart_id;
                
            }
        }
        console.log(token);
        //fetch the session data from token (eunimart id)
     var sessionData=await Session.findOne({"cartToken":token});
     console.log(sessionData);
     //based on the data selected actions are triggered and API's are called and responses are returned
     if(sessionData.vas_code.length>0){
       var resp= await vas(sessionData.account_id,sessionData.client_token);
       console.log(resp);
       var vas_names=[];
       for (var i = 0; i <sessionData.vas_code.length ; i++) {
       for (var j = 0; j<resp.data.length ; j++) {
        if(sessionData.vas_code[i]===resp.data[j].vas_code) {
            vas_names.push(resp.data[j].name);
        }
    }
}
    var vas_name=","+"VAS:"+" "+vas_names.toString();
    console.log(vas_names.toString());
       var selected_vas=[]
       for (var i = 0; i <sessionData.vas_code.length ; i++) {
        for (var j = 0; j <resp.data.length ; j++){
            if(sessionData.vas_code[i]===resp.data[j].vas_code){
                selected_vas.push(resp.data[j])
            }
        }
    }
    //calculate vas price
    var vas_price_array=selected_vas.map(function(val){ 
        return val.rate
    }) 
     vas_price_value=vas_price_array.reduce((a, b) => a + b, 0)
     }else{
        var vas_price_value=0;
        vas_name=""
     }
     console.log(sessionData);
     if(sessionData.service_type=="drop_off"){
         console.log(vas_name)
         var drop_off=[];
         let response=await pickUpPoints(sessionData.account_id,sessionData.client_token,sessionData.city_id,sessionData.district_id,sessionData.country_id)
         for (var i = 0; i <response.data.length ; i++) {
             if(sessionData.shipping_code===response.data[i].service_provider_id){
                drop_off.push(response.data[i]);
             }
         }
         console.log(drop_off)
         return res.json({"rates":[{"service_name":"drop off"+vas_name,"service_code":"DOFF","total_price":(+drop_off[0].rate+vas_price_value)*100,"description":"Pick up point","currency":"AED" }]})
     }
     if(sessionData.service_type=="Shipping"){
       
          var res1=await serviceTypes(sessionData.account_id,sessionData.client_token,sessionData.city_id,sessionData.district_id,sessionData.country_id,sessionData.weight,sessionData.service_id,sessionData.delivery_date,sessionData.id)
          console.log(res1)
          var data=res1.data;
          console.log(data);

          var serviceRateArr=[]
          for (var i = 0; i <data.length ; i++) {
           
                if(data[i].id===sessionData.service_id){
                    console.log(data[i].id)
                    serviceRateArr.push(data[i])
                }
          }
          console.log(serviceRateArr);
        var a=  serviceRateArr.map(function(val){ 
            return {"service_name":val.name+vas_name,"service_code":val.service_type_code,"total_price":(+val.rate+vas_price_value)*100,"description":val.description,"currency":"AED" }; 
        }) 
        console.log(a)
        return res.json({"rates":a})
     }
    
    
    } catch (error) {
        console.log(error)
       
    }
}
let pickUpPoints = (account_id,client_token,city_id,district_id,country_id) => {
    var options = { 
        method: 'GET',
        url: process.env.euimart_base_url+"/api/v2/web_store_plugin/oms_services/pickup_points",
        qs: 
            {   "account_id":account_id,
            "city_id":city_id,
            "district_id":district_id,
            "country_id":country_id
            },
            headers:{
                "CLIENT-TOKEN":client_token
          },
        json: true
    };

    return request(options)

}


let serviceTypes = (account_id,client_token,city_id,district_id,country_id,weight,service_id,delivery_date,id) => {
    var options = { 
        method: 'GET',
        url: process.env.euimart_base_url+"/api/v2/web_store_plugin/oms_services/service_types",
        qs: 
            {   "account_id":account_id,
            "country_id":country_id,
            "city_id":city_id,
            "district_id":district_id,
            "weight":weight,
            "service_id":id
           },
            headers:{
                "CLIENT-TOKEN":client_token
          },
        json: true
    };
console.log(options)
    return request(options)

}
let vas = (account_id,client_token) => {
    var options = { 
        method: 'GET',
        url: process.env.euimart_base_url+"/api/v2/web_store_plugin/oms_services/vas",
        qs: 
            {   "account_id":account_id
            },
            headers:{
                "CLIENT-TOKEN":client_token
          },
        json: true
    };

    return request(options)

}

module.exports = {
    carrierApi
  };