const request = require('request-promise');
const Shop =require('../model/shop');
const dotenv = require("dotenv").config();
//all eunumart api's are integrated here

let addedServices=async(req,res)=>{
    console.log("post")
    const { shopName } =req.body;
   
 
    try {
   let shopdetails=await Shop.findOne({"shopName":shopName});
//    if(shopdetails.install=="uninstalled"){
//        return res.json({"success":false,"msg":"Invalid shop details"})
//    }

let response=await vas(shopdetails);
if(response.status===true){
    return  res.json(response);
}else{
    res.json({
        "success": false,
        "msg": 'No services available'
        });
}
    } catch (error) {
        res.json({
            "success": false,
            "msg": 'Server Error'
            });
    }
}

let check_location=async(req,res)=>{
    console.log("post")
    const { shopName,country_name,city_name,district_name } =req.body;
   
 
    try {
   let shopdetails=await Shop.findOne({"shopName":shopName});
   if(shopdetails.install=="uninstalled"){
    return res.json({"success":false,"msg":"Invalid shop details"})
}
let response=await checkGeoLocation(shopdetails,country_name,city_name,district_name);
if(response.status===true){
    return  res.json(response);
}else{
    res.json({
        "success": false,
        "msg": 'No services available'
        });
}
    } catch (error) {
        res.json({
            "success": false,
            "msg": 'Server Error'
            });
    }
}
let country_list=async(req,res)=>{
    console.log("post")
    const { shopName,country_name,city_name,district_name } =req.body;
   
 
    try {
   let shopdetails=await Shop.findOne({"shopName":shopName});
//    if(shopdetails.install=="uninstalled"){
//     return res.json({"success":false,"msg":"Invalid shop details"})
// }
let response=await listCountries(shopdetails);
if(response.status===true){
    return  res.json(response);
}else{
    res.json({
        "success": false,
        "msg": 'An error occured'
        });
}
    } catch (error) {
        res.json({
            "success": false,
            "msg": 'Server Error'
            });
    }
}
let city_list=async(req,res)=>{
    console.log("post")
    const { shopName,city_name } =req.body;
   
 
    try {
   let shopdetails=await Shop.findOne({"shopName":shopName});
//    if(shopdetails.install=="uninstalled"){
//     return res.json({"success":false,"msg":"Invalid shop details"})
// }
let response=await listCities(shopdetails,city_name);
if(response.status===true){
    return  res.json(response);
}else{
    res.json({
        "success": false,
        "msg": 'No services available'
        });
}
    } catch (error) {
        res.json({
            "success": false,
            "msg": 'Server Error'
            });
    }
}
let district_list=async(req,res)=>{
      console.log("post")
    const { shopName,district_name } =req.body;
   
 
    try {
   let shopdetails=await Shop.findOne({"shopName":shopName});
//    if(shopdetails.install=="uninstalled"){
//     return res.json({"success":false,"msg":"Invalid shop details"})
// }
let response=await listDistrict(shopdetails,district_name);
if(response.status===true){
    return  res.json(response);
}else{
    res.json({
        "success": false,
        "msg": 'No services available'
        });
}
    } catch (error) {
        res.json({
            "success": false,
            "msg": 'Server Error'
            });
    }
    
}
let pick_up_points=async(req,res)=>{
    console.log("post")
    const { shopName,city_id,district_id,country_id,pageNo } =req.body;
   
 
    try {
   let shopdetails=await Shop.findOne({"shopName":shopName});

let response=await pickUpPoints(shopdetails,city_id,district_id,country_id,pageNo);
if(response.status===true){
    return  res.json(response);
}else{
    res.json({
        "success": false,
        "msg": 'No services available'
        });
}
    } catch (error) {
        res.json({
            "success": false,
            "msg": 'Server Error'
            });
    }
  
}
let service_types=async(req,res)=>{
    console.log("post")
    const { shopName,city_id,district_id,country_id,weight,service_id,delivery_date } =req.body;
   
 
    try {
   let shopdetails=await Shop.findOne({"shopName":shopName});
//    if(shopdetails.install=="uninstalled"){
//     return res.json({"success":false,"msg":"Invalid shop details"})
// }
let response=await serviceTypes(shopdetails,city_id,district_id,country_id,weight,service_id,delivery_date);
if(response.status===true){
    return  res.json(response);
}else{
    res.json({
        "success": false,
        "msg": 'No services available'
        });
}
    } catch (error) {
        res.json({
            "success": false,
            "msg": 'Server Error'
            });
    }
  
}
let services=async(req,res)=>{
    console.log("post")
    const { shopName,city_id,district_id,country_id,weight } =req.body;
   
 
    try {
   let shopdetails=await Shop.findOne({"shopName":shopName});
//    if(shopdetails.install=="uninstalled"){
//     return res.json({"success":false,"msg":"Invalid shop details"})
// }
let response=await services1(shopdetails,city_id,district_id,country_id,weight);
if(response.status===true){
    return  res.json(response);
}else{
    res.json({
        "success": false,
        "msg": 'No services available'
        });
}
    } catch (error) {
        console.log(error)
        res.json({
            "success": false,
            "msg": 'Server Error'
            });
    }
  
}

let checkGeoLocation = (shopdetails,country_name,city_name,district_name) => {
    var options = { 
        method: 'GET',
        url: process.env.euimart_base_url+"/api/v2/web_store_plugin/oms_services/geo_location/check",
        qs: 
            {   account_id:shopdetails.account_id,
            country_name: country_name,
            city_name: city_name,
            district_name:district_name
            },
            headers:{
                "CLIENT-TOKEN":shopdetails.client_token
          },
        json: true
    };

    return request(options)

}
let listCountries = (shopdetails) => {
    var options = { 
        method: 'GET',
        url: process.env.euimart_base_url+"/api/v2/web_store_plugin/oms_services/geo_location/country",
        qs: 
            {   "account_id":shopdetails.account_id,
            "country_name":"uae"
            },
            headers:{
                "CLIENT-TOKEN":shopdetails.client_token
          },
        json: true
    };

    return request(options)

}
let listDistrict = (shopdetails,district_name) => {
    var options = { 
        method: 'GET',
        url: process.env.euimart_base_url+"/api/v2/web_store_plugin/oms_services/geo_location/districts",
        qs: 
            {   "account_id":shopdetails.account_id,
            "district_name":district_name
            },
            headers:{
                "CLIENT-TOKEN":shopdetails.client_token
          },
        json: true
    };

    return request(options)

}
let vas = (shopdetails) => {
    var options = { 
        method: 'GET',
        url: process.env.euimart_base_url+"/api/v2/web_store_plugin/oms_services/vas",
        qs: 
            {   "account_id":shopdetails.account_id
            },
            headers:{
                "CLIENT-TOKEN":shopdetails.client_token
          },
        json: true
    };

    return request(options)

}
let listCities = (shopdetails,city_name) => {
    var options = { 
        method: 'GET',
        url: process.env.euimart_base_url+"/api/v2/web_store_plugin/oms_services/geo_location/cities",
        qs: 
            {   "account_id":shopdetails.account_id,
            "city_name":city_name
            },
            headers:{
                "CLIENT-TOKEN":shopdetails.client_token
          },
        json: true
    };

    return request(options)

}
let pickUpPoints = (shopdetails,city_id,district_id,country_id,pageNo) => {
    var options = { 
        method: 'GET',
        url: process.env.euimart_base_url+"/api/v2/web_store_plugin/oms_services/pickup_points",
        qs: 
            {   "account_id":shopdetails.account_id,
            "city_id":city_id,
            "district_id":district_id,
            "country_id":country_id,
            "pageNo":pageNo
            },
            headers:{
                "CLIENT-TOKEN":shopdetails.client_token
          },
        json: true
    };

    return request(options)

}
let serviceTypes = (shopdetails,city_id,district_id,country_id,weight,service_id,delivery_date) => {
    var options = { 
        method: 'GET',
        url: process.env.euimart_base_url+"/api/v2/web_store_plugin/oms_services/service_types",
        qs: 
            {   "account_id":shopdetails.account_id,
            "country_id":country_id,
            "city_id":city_id,
            "district_id":district_id,
            "weight":weight,
            "service_id":service_id,
            "delivery_date":delivery_date
           
            },
            headers:{
                "CLIENT-TOKEN":shopdetails.client_token
          },
        json: true
    };

    return request(options)

}
let services1 = (shopdetails,city_id,district_id,country_id,weight) => {
    var options = { 
        method: 'GET',
        url: process.env.euimart_base_url+"/api/v2/web_store_plugin/oms_services/services",
        qs: 
            {   "account_id":shopdetails.account_id,
            "country_id":country_id,
            "city_id":city_id,
            "district_id":district_id,
            "weight":weight
           },
            headers:{
                "CLIENT-TOKEN":shopdetails.client_token
          },
        json: true
    };

    return request(options)

}









module.exports={
    check_location,
    country_list,
    city_list,
    district_list,
    pick_up_points,
    service_types,
    services,
    addedServices
}
