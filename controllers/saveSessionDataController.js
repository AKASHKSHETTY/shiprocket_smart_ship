const Shop=require('../model/shop')
const Session =require('../model/session');
const dotenv = require("dotenv").config();
let saveSessionData=async(req,res)=>{
 //al the session datas are stored here based on what user has selected
    const { cartToken,country_id,id,
        city_id,
        district_id,
        weight,
        service_id,
        delivery_date ,service_type,shopName,vas_code,shipping_code} =req.body;
   
   
 
    try {
        if(!cartToken) return res.status(400).send(
            "cart token missing"
          );
        let ShopDetails=await Shop.findOne({"shopName":shopName});
        let date=new Date()
        if(ShopDetails.install=="uninstalled"){
            return res.json({"success":false,"msg":"Invalid shop details"})
        }
        var account_id=ShopDetails.account_id;
        var client_token=ShopDetails.client_token;
        let prevSession=await Session.find({"cartToken":cartToken});
        if(prevSession.length>0){
            await Session.findOneAndUpdate({
                "cartToken":cartToken
            },{
                "country_id":country_id,
                "city_id":city_id,
                "district_id":district_id,
                "weight":weight,
                "service_id":service_id,
                "delivery_date":delivery_date,
                "service_type":service_type,
                "id":id,
                "vas_code":vas_code,
                "date":date,
                "shipping_code":shipping_code
            }, {
                upsert: true
            }
            )
            return res.json({"success":true,"msg":"session updated successfully"});
        }
        session=new Session({
            cartToken,
            service_type,
            account_id,
            client_token,
            country_id,
            city_id,
            district_id,
            weight,
            service_id,
            delivery_date,
            vas_code,
            date,
            shipping_code
           });
           await session.save();
         return  res.json({
               "success":true,
               "msg":"session saved successfully"
           })
       
    } catch (error) {
        console.log(error);
        res.json({
            "success": false,
            "msg": 'Server Error'
            });
    }
}
module.exports={
    saveSessionData
}