// const {
//     Orders
// } = require('../models/orders');
const dotenv = require("dotenv").config();
const express = require("express");
const app = express();
const crypto = require("crypto");
const cookie = require("cookie");
var path = require("path");
const nonce = require("nonce")();
const querystring = require("querystring");
const request = require("request-promise");
const Shopify = require("shopify-api-node");
const _ = require("lodash");

const apiKey = process.env.SHOPIFY_API_KEY;
const apiSecret = process.env.SHOPIFY_API_SECRET;
const scopes = 'read_orders, write_orders,write_shipping,read_themes,write_themes';
const forwardingAddress = process.env.BASE_URL; //"https://marmeto2.ngrok.io";
//const forwardingAddress = "https://b306b515.ngrok.io";
//Loads the handlebars module
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');



let store = require("../model/shop");


var shopifyAPI;

/**
 * Install
 * @param {Request} req
 * @param {Response} res
 */
let shopify = async (req, res) => {
  const shop = req.query.shop;
  console.log(shop);
  var shopDatails=await store.findOne({"shopName":shop});
 
  if (shop) {
    const state = nonce();
    const redirectUri = forwardingAddress + "/shopify/callback";
    const installUrl =
      "https://" +
      shop +
      "/admin/oauth/authorize?client_id=" +
      apiKey +
      "&scope=" +
      scopes +
      "&state=" +
      state +
      "&redirect_uri=" +
      redirectUri;
//generation app installation url
    res.cookie("state", state);
    res.redirect(installUrl);
  } else {
    return res
      .status(400)
      .send(
        "Missing shop parameter. Please add ?shop=your-development-shop.myshopify.com to your request"
      );
  }
};

/**
 * Callback
 * @param {Request} req
 * @param {Response} res
 */
let callback = async (req, res) => {
  const { shop, hmac, code, state } = req.query;
  console.log("callback")
  

  if (shop && hmac && code) {
    // DONE: Validate request is from Shopify
    const map = Object.assign({}, req.query);
    delete map["signature"];
    delete map["hmac"];
    const message = querystring.stringify(map);
    const providedHmac = Buffer.from(hmac, "utf-8");
    const generatedHash = Buffer.from(
      crypto
        .createHmac("sha256", apiSecret)
        .update(message)
        .digest("hex"),
      "utf-8"
    );
    let hashEquals = false;

    try {
      hashEquals = crypto.timingSafeEqual(generatedHash, providedHmac);
    } catch (e) {
      hashEquals = false;
    }

    if (!hashEquals) {
      return res.status(400).send("HMAC validation failed");
    }

    // DONE: Exchange temporary code for a permanent access token
    const accessTokenRequestUrl =
      "https://" + shop + "/admin/oauth/access_token";
    const accessTokenPayload = {
      client_id: apiKey,
      client_secret: apiSecret,
      code
    };

    request
      .post(accessTokenRequestUrl, {
        json: accessTokenPayload
      })
      .then(accessTokenResponse => {
        const accessToken = accessTokenResponse.access_token;
        // DONE: Use access token to make API call to 'shop' endpoint
        const shopRequestUrl =
          "https://" + shop + "/admin/api/2019-04/shop.json";
        const shopRequestHeaders = {
          "X-Shopify-Access-Token": accessToken
        };

        request
          .get(shopRequestUrl, {
            headers: shopRequestHeaders
          })
          .then(async shopResponse => {
            shopifyAPI = new Shopify({
              shopName: shop,
              accessToken: accessToken,
              apiVersion: '2020-01'
            });

            console.log("shop - " + shop);
         

            await store
              .update(
                {
                  shopName: shop
                },
                {
                  shopName: shop,
                  access_token: accessToken,
                  install:"first"
                },
                {
                  upsert: true
                }
              )
              .then(result => {
                console.log(result.nModified);
                if (result.upserted || result.nModified == 1) {
                  mainPage(res, req, shopifyAPI, "first");
                } else {
                  mainPage(res, req, shopifyAPI, "second");
                }
              })
              .catch(err => {
                console.log(err);
              });
          })
          .catch(error => {
            res.status(500).send(error);
          });
      })
      .catch(error => {
        res.status(error.statusCode).send(error);
      });
  } else {
    res.status(400).send("Required parameters missing");
  }
};

/**
 * Activate
 * @param {Request} req
 * @param {Response} res
 */
let activate = async (req, res) => {
  // console.log(typeof req.query.charge_id);

  shopifyAPI.recurringApplicationCharge
    .activate(req.query.charge_id, {})
    .then(done => confirm(res, shopifyAPI, "first"));
};

async function pending(res, shopifyAPI) {
  confirm(res, shopifyAPI, "second");
}

function accepted(res, shopifyAPI) {
  confirm(res, shopifyAPI, "second");
}

async function mainPage(res, req, shopifyAPI, install) {
  registerWebhooks(shopifyAPI);
  deleteWebhooks(shopifyAPI);
  confirm(res, req, shopifyAPI, install);
}

async function confirm(res, req, shopName, install) {
  try {
  console.log("in the confirm")
    req.session.url = shopName.options.shopName;
    res.locals.session = req.session;
    req.session.save();
  
    if (install === "first") {
     
      res.redirect("https://"+shopName.options.shopName+"/admin");
    } else {
      var shop=await store.findOne({"shopName":shopName.options.shopName});
      if(!shop.account_id||!shop.client_token){
        shop.account_id=null;
        shop.client_token=null
      }
      return res.render('main/home',{
        "shopName":shopName.options.shopName,
        "account_id":shop.account_id,
        "client_token":shop.client_token
      });
     }
  } catch (error) {
    console.log(error)
  }
 
}



/**
 * insert script in customer page
 * @param {Request} req
 * @param {Response} res
 */

//resigter webhook
function registerWebhooks_(shopifyAPI, topic) {
  let webhookEndpoint = topic.replace("/", "-");
  let BASE_URL = process.env.BASE_URL;
  let address = `${BASE_URL}/webhooks/${webhookEndpoint}`;
  return shopifyAPI.webhook.create({
    topic: topic,
    address: address,
    format: "json"
  });
}

//check for webhook is registered on not
async function checkWebhookExist(shopifyAPI, topic) {
  var exist = false;
  var webhookList = await shopifyAPI.webhook.list();
  for (var i = 0; i < webhookList.length; i++) {
    if (webhookList.topic === topic) {
      exist = true;
      break;
    }
  }
  return exist;
}
//pass the webhook topic to resigter webhook
async function registerWebhooks(shopifyAPI) {
  var args = [
    "orders/cancelled",
    "orders/create",
    "orders/updated",
    "orders/delete",
    "app/uninstalled"
  ];
  for (let i = 0; i < args.length; i++) {
    if (await checkWebhookExist(shopifyAPI, args[i])) {
      console.log("webhook exist");
    } else {
      try {
        let result = await registerWebhooks_(shopifyAPI, args[i]);
        console.log(result);
      } catch (err) {
        // console.log(err);
        // console.log('Error; statusCode: ', err.statusCode);
        // break;
      }
    }
  }
}

async function registerWebhooksViaApi(req, res, name) {
  var args = [
    "orders/cancelled",
    "orders/create",
    "orders/updated",
    "orders/delete",
    "app/uninstalled"
  ];
  let store_name = req.query.store;
  console.log("store url - "+store_name);
  var shop = await Store.find({
      "shop_name": store_name
  })

  let shopify = new Shopify({
      shopName: shop[0].shop_name,
      accessToken: shop[0].access_token,
      apiVersion: '2020-01'
  });
  for (let i = 0; i < args.length; i++) {
    if (await checkWebhookExist(shopify, args[i])) {
      console.log("webhook exist");
    } else {
      try {
        let result = await registerWebhooks_(shopify, args[i]);
        res.send(result)
      } catch (err) {
        // console.log(err);
        // console.log('Error; statusCode: ', err.statusCode);
        // break;
      }
    }
  }
}

async function listWebhooksViaApi(req, res, next) {
  let store_name = req.query.store;
  console.log("store url - "+store_name);
  var shop = await Store.find({
      "shop_name": store_name
  })

  let shopify = new Shopify({
      shopName: shop[0].shop_name,
      accessToken: shop[0].access_token,
      apiVersion: '2020-01'
  });

  let result = await shopify.webhook.list();
  res.send(result);
}
//delete webhook
async function deleteWebhooks(shopifyAPI) {
  var args = ["668647391292", "668647424060", "668647456828", "668647489596"];
  for (let i = 0; i < args.length; i++) {
    try {
      let result = await deleteWebhook(shopifyAPI, args[i]);
      console.log(result);
    } catch (err) {
      // console.log('Error; statusCode: ', err.statusCode);
    }
  }
}

//delete webhook
function deleteWebhook(shopifyAPI, id) {
  return shopifyAPI.webhook.delete(id);
}
async function registerWebhooksViaApi(req, res, name) {
  var args = [
    "orders/cancelled",
    "orders/create",
    "orders/updated",
    "orders/delete",
    "app/uninstalled"
  ];
  let store_name = req.query.store;
  console.log("store url - "+store_name);
  var shop = await store.find({
      "shop_name": store_name
  })

  let shopify = new Shopify({
      shopName: shop[0].shop_name,
      accessToken: shop[0].access_token,
      apiVersion: '2020-01'
  });
  for (let i = 0; i < args.length; i++) {
    if (await checkWebhookExist(shopify, args[i])) {
      console.log("webhook exist");
    } else {
      try {
        let result = await registerWebhooks_(shopify, args[i]);
        res.send(result)
      } catch (err) {
        // console.log(err);
        // console.log('Error; statusCode: ', err.statusCode);
        // break;
      }
    }
  }
}

async function listWebhooksViaApi(req, res, next) {
  let store_name = req.query.store;
  console.log("store url - "+store_name);
  var shop = await store.find({
      "shop_name": store_name
  })

  let shopify = new Shopify({
      shopName: shop[0].shop_name,
      accessToken: shop[0].access_token,
      apiVersion: '2020-01'
  });

  let result = await shopify.webhook.list();
  res.send(result);
}

async function deleteAllWebhooksViaApi(req, res, next) {
  try {
    let store_name = req.body.store;
    
    var shop = await store.find({
      "shop_name": store_name
    });

    let shopify = new Shopify({
      shopName: shop[0].shop_name,
      accessToken: shop[0].access_token,
      apiVersion: '2020-01'
    });

    let webhooks = await shopify.webhook.list();

    for (let i = 0; i < webhooks.length; i++) {
      let del_webhook = await shopify.webhook.delete(webhooks[i].id);
    }

    webhooks = await shopify.webhook.list();
    console.log(`total webhooks deleted for ${store_name} are ${webhooks.length}`);
    res.send(webhooks);
  } catch (error) {
    console.error(':: ERROR IN deleteAllWebhooksViaApi() ::');
    console.error(error);
  }
};

module.exports = {
  shopify,
  callback,
  registerWebhooksViaApi,
  listWebhooksViaApi,
  deleteAllWebhooksViaApi,
  registerWebhooksViaApi,
  listWebhooksViaApi,
  activate
};
