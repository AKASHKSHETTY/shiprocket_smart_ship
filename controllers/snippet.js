`<style>.eunimart-select{position:relative;max-width:300px;outline:none}.eunimart-select .value{display:block;position:relative;font-family:helvetica,arial,sans-serif;font-size:14px;color:#1f202a;;padding:8px 10px;border:1px solid #dadada;border-radius:5px;cursor:pointer;overflow-x:hidden;white-space:nowrap}.eunimart-select .value::before{position:absolute;top:50%;right:0;margin:-2px 6px;width:0px;height:0px;content:"";border-width:5px;border-style:solid;border-color:#787878 transparent transparent}.eunimart-select:focus .value{border-bottom:1px solid #e3af43}.eunimart-select .list{position:absolute;top:0;width:100%;height:0;background:#fff;opacity:0;outline:none;-webkit-box-shadow:0px 2px 9px rgba(0,0,0,0.35);box-shadow:0px 2px 9px rgba(0,0,0,0.35);overflow:hidden;-webkit-transition:opacity 0.25s cubic-bezier(0.39, 0.575, 0.565, 1);transition:opacity 0.25s cubic-bezier(0.39, 0.575, 0.565, 1);z-index:9999}.eunimart-select .list.open{height:auto;opacity:1.0}.eunimart-select .list .filter{display:none;padding:8px 10px}.eunimart-select .list .filter input{width:100%;font-family:helvetica,arial,sans-serif;font-size:12px;color:#1f202a;border:none;border-bottom:1px solid #eaeaea;outline:none;margin:0;padding:0 0 5px}.eunimart-select .list ul{display:block;margin:0;padding:0}.eunimart-select .list ul li{display:block;list-style:none;font-family:helvetica,arial,sans-serif;font-size:12px;color:#1f202a;padding:8px 10px;cursor:pointer}.eunimart-select .list ul li:hover,.eunimart-select .list ul li.hovered{color:#fff;background:#e3af43}.eunimart-select.large .filter{display:block}.eunimart-select.large .list ul{max-height:200px;overflow-y:scroll}</style>
<style>
  .city__input-wrapper{max-width:50%}.city__header{font-weight:600;letter-spacing:1px;font-size:15px;line-height:1.6}#serviceContent{display:flex;flex-direction:column;max-width:50%}.serviceMethod{display:flex}#map{height:240px;width:500px;margin-bottom:30px}.mm-service{display:inline-block;width:145px;border:1px solid #dadada;border-radius:5px;margin-bottom:20px;margin-right:10px;padding:.5em;text-align:center}.mm-service:hover{cursor:pointer}.mm-service.active{background-color:#dcdcdc;border-color:#000}.service{width:300px;border:1px solid #dadada;border-radius:5px;margin-top:.5em;margin-bottom:1em;max-height:215px;overflow:auto}.service_type{padding:1em;line-height:1.2em;border-bottom:1px solid #f3f3f3}.service_type.fix{background:#eee}.picup-location{width:500px;border:1px solid #dadada;border-radius:5px;margin-top:.5em;margin-bottom:1em;max-height:200px;overflow:auto}.picup-point{display:flex;padding:1em;line-height:1.2em;border-bottom:1px solid #f3f3f3}.picup-point input[type=radio]{margin-top:.1em;margin-right:.5em}.picup-point.fix{background:#eee}.datepicker{width:300px;padding:.7em;border:1px solid #dadada;background:#eee;margin-bottom:1em}.iconContent{line-height:1em;font-size:.9em}.serviceIcon{padding-bottom:.5em}.service+.vlabel+.vas{width:300px;border:1px solid #dadada;border-radius:5px;margin-top:.5em;margin-bottom:1em}.picup-location+.vlabel+.vas{width:500px;border:1px solid #dadada;border-radius:5px;margin-top:.5em;margin-bottom:1em}.datepicker+.vlabel+.vas{width:300px;border:1px solid #dadada;border-radius:5px;margin-top:.5em;margin-bottom:1em}.vas_type{padding:1em;line-height:1.2em;border-bottom:1px solid #f3f3f3}.vas_type input[type=checkbox]{margin-top:.1em;margin-right:.7em}.sblock{display:inline-flex;flex-direction:column}.picup-point label{display:flex;font-size:1em;text-transform:capitalize;cursor:pointer}.eunimart-error{color:#c00}@media only screen and (max-width:749px){.city__input-wrapper{max-width:350px}.city__header{font-size:14px}#serviceContent{display:flex;flex-direction:column;max-width:350px}.serviceMethod{display:flex;width:350px}.serviceMethodContainer{width:300px}#map{height:240px;width:300px;margin-bottom:30px}.mm-service{display:inline-block;width:145px;border:1px solid #dadada;border-radius:5px;margin-bottom:20px;margin-right:10px;padding:.5em;text-align:center}.mm-service:hover{cursor:pointer}.mm-service.active{background-color:#dcdcdc;border-color:#000}.service{width:300px;border:1px solid #dadada;border-radius:4px;margin-top:.5em;margin-bottom:1em;max-height:215px;overflow:auto}.service_type{padding:1em;line-height:1.2em;border-bottom:1px solid #f3f3f3}.service_type.fix{background:#eee}.picup-location{width:300px;border:1px solid #dadada;border-radius:4px;margin-top:.5em;margin-bottom:1em;max-height:200px;overflow:auto}.picup-point{display:flex;padding:1em;line-height:1.2em;border-bottom:1px solid #f3f3f3}.picup-point input[type=radio]{margin-top:.1em;margin-right:.5em}.picup-point.fix{background:#eee}.datepicker{width:300px;padding:.7em;border:1px solid #dadada;background:#eee}.iconContent{line-height:1em;font-size:.9em}.serviceIcon{padding-bottom:.5em}.service+.vlabel+.vas{width:300px;border:1px solid #dadada;border-radius:4px;margin-top:.5em;margin-bottom:1em}.picup-location+.vlabel+.vas{width:300px;border:1px solid #dadada;border-radius:4px;margin-top:.5em;margin-bottom:1em}.datepicker+.vlabel+.vas{width:300px;border:1px solid #dadada;border-radius:4px;margin-top:.5em;margin-bottom:1em}.vas_type{padding:1em;line-height:1.2em;border-bottom:1px solid #f3f3f3}.vas_type input[type=checkbox]{margin-top:.1em;margin-right:.7em}.sblock{display:inline-flex;flex-direction:column}.picup-point label{font-size:1em;text-transform:capitalize;cursor:pointer}.eunimart-error{color:#c00}}
</style>

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAEP56iLf-3bxjxUenTPFaTZveXmkH6a2c" defer></script>
<script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>

<script>var Select=function(t,i){this.target=null,this.select=null,this.display=null,this.list=null,this.options=[],this.isLarge=!1,this.value=null,this.selected=null,this.settings=null,this.highlighted=null,this.init=function(){switch(typeof t){case"object":this.target=t;break;case"string":this.target=document.querySelector(t)}this.settings=this.getSettings(i),this.buildSelect(),this.target.parentNode.replaceChild(this.select,this.target),this.target.style.display="none",this.select.appendChild(this.target),document.addEventListener("click",this.handleClickOff.bind(this)),this.positionList()},this.buildSelect=function(){this.select=document.createElement("div"),this.select.classList.add("eunimart-select"),this.select.setAttribute("tabindex",this.target.tabIndex),this.select.addEventListener("keydown",this.handleSelectKeydown.bind(this)),this.display=document.createElement("span"),this.display.classList.add("value"),this.display.addEventListener("click",this.handleDisplayClick.bind(this)),this.select.appendChild(this.display),this.buildList(),this.options.length&&(this.value=this.options[this.target.selectedIndex].getAttribute("data-value"),this.selected=this.options[this.target.selectedIndex],this.display.innerHTML=this.selected.innerHTML),("auto"===this.settings.filtered&&this.options.length>=this.settings.filter_threshold||this.settings.filtered===!0)&&(this.isLarge=!0,this.select.classList.add("large"))},this.buildList=function(){this.list=document.createElement("div"),this.list.classList.add("list"),this.list.setAttribute("tabindex","-1"),this.list.addEventListener("keydown",this.handleListKeydown.bind(this)),this.list.addEventListener("mouseenter",function(){this.options[this.highlighted].classList.remove("hovered")}.bind(this)),this.highlighted=this.target.selectedIndex,this.buildFilter(),this.buildOptions(),this.select.appendChild(this.list)},this.buildFilter=function(){var t=document.createElement("div");t.classList.add("filter"),this.filter=document.createElement("input"),this.filter.type="text",this.filter.setAttribute("placeholder",this.settings.filter_placeholder),this.filter.addEventListener("keyup",this.handleFilterKeyup.bind(this)),t.appendChild(this.filter),this.list.appendChild(t)},this.buildOptions=function(){for(var t=document.createElement("ul"),i=this.target.querySelectorAll("option"),e=0;e<i.length;e++){var s=document.createElement("li");s.setAttribute("data-value",i[e].value),s.setAttribute("data-country",i[e].getAttribute("data-country-id")),s.setAttribute("data-district",i[e].getAttribute("data-district-id")),s.innerHTML=i[e].innerHTML,s.addEventListener("click",this.handleOptionClick.bind(this)),t.appendChild(s),this.options.push(s)}this.list.appendChild(t)},this.toggleList=function(){this.list.classList.contains("open")?(this.list.classList.remove("open"),this.options[this.highlighted].classList.remove("hovered"),this.select.focus()):(this.options[this.target.selectedIndex].classList.add("hovered"),this.highlighted=this.target.selectedIndex,this.list.classList.add("open"),this.list.focus())},this.positionList=function(){this.isLarge||(this.list.style.top="-"+this.selected.offsetTop+"px")},this.highlightOption=function(t){var i=null;switch(t){case"up":i=this.highlighted-1<0?this.highlighted:this.highlighted-1;break;case"down":i=this.highlighted+1>this.options.length-1?this.highlighted:this.highlighted+1;break;default:i=this.highlighted}this.options[this.highlighted].classList.remove("hovered"),this.options[i].classList.add("hovered"),this.highlighted=i},this.clearFilter=function(){this.filter.value="";for(var t=0;t<this.options.length;t++)this.options[t].style.display="block"},this.closeList=function(){this.list.classList.remove("open"),this.options[this.highlighted].classList.remove("hovered")},this.getSettings=function(t){var i={filtered:"auto",filter_threshold:8,filter_placeholder:"Filter options..."};for(var e in t)i[e]=t[e];return i},this.handleSelectKeydown=function(t){this.select===document.activeElement&&32==t.keyCode&&this.toggleList()},this.handleDisplayClick=function(t){this.list.classList.add("open"),this.isLarge&&this.filter.focus()},this.handleListKeydown=function(t){if(this.list===document.activeElement)switch(t.keyCode){case 38:this.highlightOption("up");break;case 40:this.highlightOption("down");break;case 13:this.target.value=this.options[this.highlighted].getAttribute("data-value"),this.selected=this.options[this.highlighted],this.display.innerHTML=this.options[this.highlighted].innerHTML,this.closeList(),setTimeout(this.positionList.bind(this),200),this.select.focus()}},this.handleFilterKeyup=function(t){var i=this;this.options.filter(function(t){t.innerHTML.substring(0,i.filter.value.length).toLowerCase()==i.filter.value.toLowerCase()?t.style.display="block":t.style.display="none"})},this.handleOptionClick=function(t){this.display.innerHTML=t.target.innerHTML,this.target.value=t.target.getAttribute("data-value"),this.value=this.target.value,this.selected=t.target,this.closeList(),this.clearFilter(),setTimeout(this.positionList.bind(this),200)},this.handleClickOff=function(t){this.select.contains(t.target)||this.closeList()},this.init()};</script>

<script>
  window.addEventListener("DOMContentLoaded",function(){
  
    const htmlData = \`<div id="shippingApp">
  						<div class="city__input-wrapper" id="city-field">
    						<label class="city__label" for="city"><p class="city__header">Select District for Eunimart Pickup Points Shipping Service</p></label>
    						<select name="city" id="city" class="city__input">
      							<option value="">Select District</option>
    						</select>
  						</div>
  						<br />
  						<div id="serviceContent">
    						<div class="serviceMethod"></div>
    						<div class="serviceMethodContainer"> </div>
  						</div>
					   </div>\`;
    
    const eunimart = document.getElementById("eunimart__wrapper");
    eunimart.innerHTML = htmlData;
    const cityField = document.getElementById("city-field");
    const cityList = document.getElementById("city");
    const serviceContent  = document.getElementById("serviceContent");
    const serviceMethod = document.querySelector(".serviceMethod");
    const ServiceMethodContainer = document.querySelector(".serviceMethodContainer");
    

    // Ajax call for City List 
    let shopName = '{{ shop.permanent_domain }}';
    let weight = parseInt('{{ cart.total_weight }}');
    if(weight === 0){
      weight = 0.1;
    }
    let vcode = [];
    let vas_id = [];
    let district_name = "";
    let pickup_location_id= "";
	
    const datax = {shopName:shopName,district_name:"India"}
    fetch("https://eunimart.minimaltrex.com/eunimart/district_list",{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(datax)
    })
    .then(function(response){
      return response.json();
    })
    .then(function(json){
      initCity(json.data);
    })
    .catch(function(err){
      console.log("Error:", err)
    });


    // function to Intialize District Option

    function initCity(cityData){
      let options = "";
      options+= \`<option value="">Select District</option>\`;
      cityData.forEach(function(city){
        options+=\`<option value="\${city.city_id}" data-country-id="\${city.country_id}" data-district-id="\${city.id}">\${city.name}</option>\`
      });
      cityList.innerHTML = options;

      let list = new Select(cityList,{
        // auto show the live filter
        filtered: 'auto',
        // auto show the live filter when the options >= 8
        filter_threshold: 8,
        // custom placeholder
        filter_placeholder: 'Filter District options...'
      });

      document.querySelector(".list").querySelector("ul").addEventListener("click",function(event){
        let city_id = event.target.getAttribute("data-value");
        let district_id = event.target.getAttribute("data-district");
        let country_id = event.target.getAttribute("data-country");
        district_name = event.target.textContent;
        
        let district_error_node = document.querySelector(".district-error");
        if(district_error_node){
          district_error_node.parentNode.removeChild(district_error_node);
        }
        
        const data4 = {shopName:shopName,city_id:city_id,district_id:district_id,country_id:country_id,weight:weight};
        
        fetch("https://eunimart.minimaltrex.com/eunimart/services",{
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data4)
        })
        .then(function(response){
          return response.json()
        })
        .then(function(data){
          initService(data.data,city_id,district_id,country_id)
        })
        .catch(function(err){
          console.log("Error:", err)
        });
      })
    }

    // Intialize Services

    function initService(data,city_id,district_id,country_id){

      while (serviceMethod.firstChild) {
         serviceMethod.removeChild(serviceMethod.firstChild);
      }
      while (ServiceMethodContainer.firstChild) {
        ServiceMethodContainer.removeChild(ServiceMethodContainer.firstChild);
      }
      
      let fragment = document.createDocumentFragment();
      
      data.forEach(function(service){
        let d = document.createElement("div");
        d.classList.add("mm-service");
        d.setAttribute("data-value",service.id);
        d.setAttribute("data-service",service.name);
        if(service.name==="Shipping"){
        	d.innerHTML=\`<div class="serviceIcon"><svg xmlns="http://www.w3.org/2000/svg" height="30" version="1" viewBox="0 0 160 128" preserveAspectRatio=""><path d="M23 3l-1 8v7l-10 1-11 3c-3 2 0 4 7 5l8 2-8 2c-6 1-7 2-8 4 0 2 1 2 7 3l8 2-6 1c-8 1-9 2-9 4s1 2 6 3l11 2 5 1v11l1 12 68 1h67V59l-11-21-10-21-5-1h-4v10h-12V16l-1-12c0-2-5-2-46-2L23 3zm113 32l3 7 8 19h-27V34h8l8 1zM23 83l-1 13v12h14l1-4c3-5 7-10 11-11 7-2 17 0 21 5l4 5 1 5h20c19 0 20-1 21-2 6-11 10-14 18-14 9 0 15 4 18 12 2 3 2 4 4 4 2-1 2-1 2-14V83l-67-1c-60 0-67 0-67 2z"></path><path d="M50 100c-6 3-9 9-7 15s6 9 12 9c9 0 13-5 13-13 0-10-10-16-18-11zm77 0c-6 4-8 12-5 18 3 4 6 6 12 6 5 0 6 0 9-3 4-5 5-10 2-15-3-7-11-10-18-6z"></path></svg></div><div class="iconContent">\${service.name}</div>\`;
        }
        else if(service.name==="Pickup Points"){
          d.innerHTML=\`<div class="serviceIcon"><svg xmlns="http://www.w3.org/2000/svg" height="30" version="1" viewBox="0 0 256 256" preserveAspectRatio=""><path d="M34 2c-3 4-2 9 1 12 2 2 3 2 93 2s91 0 93-2c4-3 4-8 1-12l-1-2H35l-1 2zM16 55L0 87l128 1 128-1-16-32-15-32H32L16 55zm49-18l2 2-8 19c-8 15-9 17-11 17s-3-1-3-3c0-3 17-36 19-36l1 1zm32 0l2 2-8 19c-8 15-9 17-11 17s-3-1-3-3c0-3 17-36 19-36l1 1zm33 0c1 1 2 4 2 17 0 17-1 21-4 21s-4-4-4-21c0-13 1-16 2-17l2-1 2 1zm31 0l10 17c9 17 10 21 5 21a254 254 0 0 1-21-35c0-2 2-4 4-4l2 1zm33 1c3 3 17 32 17 34s-1 3-3 3-3-2-10-17c-9-17-10-20-8-21s3-1 4 1zM0 104c1 8 1 10 4 15 3 6 10 12 16 15l4 1v104H14c-9 1-10 1-12 3-3 4-3 9 1 12 2 2 2 2 125 2s123 0 125-2c4-3 4-8 1-12-2-2-3-2-12-3h-10V135l4-1c6-3 13-9 17-15 2-5 3-7 3-15v-9H0v9zm65 9c0 3 6 12 10 15l9 6 4 1v104H40V135l4-1 9-6c4-3 10-12 10-15l1-2 1 2zm132 8c3 5 9 11 15 13l4 1v104H96V136h5c10-2 20-9 24-19l3-5 3 6c3 6 9 12 16 15 3 2 6 3 13 3s10-1 13-3c7-3 13-9 17-16l2-6 2 3 3 7z"></path><path d="M68 160c-4 2-6 10-2 14 2 1 2 3 2 9 0 7 0 9-2 11-3 3-3 9 1 12 5 4 13 1 13-6l-2-6c-2-2-2-4-2-11 0-6 0-8 2-10 3-2 3-8 0-11-2-2-7-3-10-2zM145 162l-11 11 3 3 3 3 12-11 11-12-3-3-3-3-12 12zM157 173l-23 23 3 3 4 3 23-23 22-23-2-3-4-3-23 23zM168 183l-11 13 3 3 3 3 11-12 12-12-3-3-4-3-11 11z"></path></svg></div><div class="iconContent">\${service.name}</div>\`;
        }
        else{
          d.innerHTML = \`\${service.name}\`;
        }
        fragment.appendChild(d);
      });
      serviceMethod.appendChild(fragment);


      let s_input = document.getElementsByClassName("mm-service");
      s_input = Array.from(s_input);
 
      s_input.forEach(function(i){
        i.addEventListener("click",function () {
          while (ServiceMethodContainer.firstChild) {
            ServiceMethodContainer.removeChild(ServiceMethodContainer.firstChild);
          }
          
          let serviceId = i.getAttribute("data-value");
          let service_type = i.getAttribute("data-service");
          let elems = document.querySelector(".active");
          if(elems !==null){
            elems.classList.remove("active");
          }
          i.classList.add("active");
		  
          
          if(service_type === "Pickup Points"){
            service_type = "drop_off";
            const dataP = {shopName:shopName,city_id:city_id,district_id:district_id,country_id:country_id,pageNo:"1"}
            fetch("https://eunimart.minimaltrex.com/eunimart/pick_up_points",{
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(dataP)
            })
            .then(function(response){
              return response.json()
            })
            .then(function(data){
              picupPoint(data.data,service_type,serviceId,city_id,district_id,country_id)
            })
            .catch(function(err){
              console.log("Error:", err)
            });
          }

          if(service_type === "Shipping"){
            const data5 = {shopName:shopName,city_id:city_id,district_id:district_id,country_id:country_id,weight:weight,service_id:serviceId};
            
            fetch("https://eunimart.minimaltrex.com/eunimart/service_types",{
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data5)
            })
            .then(function(response){
              return response.json()
            })
            .then(function(data){
              servType(data.data,service_type,serviceId,city_id,district_id,country_id)
            })
            .catch(function(err){
              console.log("Error:", err)
            })
          } 
        });
      });
    }
    
    // Changes Form Action
    function formAction(type,s_add){
      let eadd = s_add.split(",");
      if(type == "Shipping"){
        if(eadd[1] == undefined)
          eadd[1] = "";
        if(eadd[2] == undefined)
          eadd[2] = "";
        if(eadd[5] == undefined)
          eadd[5] = "";
        if(eadd[6] == undefined)
          eadd[6] = "";
      }
      else{
        if(eadd[6] == "")
          eadd[6] = "00000";
      }
      
      document.querySelector("form[action*='cart']").action = 
        "/cart?step=contact_information&method="+type+"&checkout%5Bshipping_address%5D%5Bfirst_name%5D=&checkout%5Bshipping_address%5D%5Blast_name%5D=&checkout%5Bshipping_address%5D%5Bcompany%5D=&checkout%5Bshipping_address%5D%5Baddress1%5D="+eadd[1]+"&checkout%5Bshipping_address%5D%5Baddress2%5D="+eadd[2]+"&checkout%5Bshipping_address%5D%5Bcity%5D="+eadd[5]+"&checkout%5Bshipping_address%5D%5Bcountry%5D=&checkout%5Bshipping_address%5D%5Bzip%5D="+eadd[6]+"&checkout%5Bshipping_address%5D%5Bprovince%5D=&locale=en"
       
    }

    function picupPoint(data,service_type,s_id,city_id,district_id,country_id){
      let input = "";

      let m = document.createElement("div");
      m.id="map";
      m.innerHTML="<p>Map</p>";
      ServiceMethodContainer.appendChild(m);
      
      initMap(data);
      
      let pmsg = document.createElement("p");
      pmsg.classList.add("add-msg");
      pmsg.innerHTML="Please ignore address on checkout:";
      ServiceMethodContainer.appendChild(pmsg);
		
      let p = document.createElement("div");
      p.classList.add("picup-location");
      
      let fragment = document.createDocumentFragment();
      
      data.forEach(function(pic){
        let d = document.createElement("div");
        d.classList.add("picup-point");
        d.setAttribute("data-value",pic.id);
        d.setAttribute("data-type",pic.pickup_point_type);
        d.setAttribute("data-lat",pic.latitude);
        d.setAttribute("data-long",pic.longitude);
        d.setAttribute("data-scode",pic.service_provider_id);
        d.setAttribute("data-lcode",pic.location_id);
        d.innerHTML=\`<label><input type="radio" name = "stype" value="\${pic.id}" data-type="\${pic.pickup_point_type}"> <span class="sblock"><span><strong>Pickup-Point</strong></span><span class="sadd">\${pic.location_name},\${pic.address_1},\${pic.address_2},<br>\${pic.address_3},\${pic.street_name},\${pic.city_name},\${pic.pin_code}</span></span></label> \`;
        fragment.appendChild(d);
      });
     
      p.appendChild(fragment);
      ServiceMethodContainer.appendChild(p);
	
      let delivery_date = "";
      
      let pp = document.getElementsByClassName("picup-point");
      pp = Array.from(pp);
      
      pp.forEach(function(i){
        i.addEventListener("change",function(){
          let ele = document.querySelector(".fix");
          if(ele !==null){
            ele.classList.remove("fix");
          }
          
          i.classList.add("fix");
          
          
          let valueLabel  = document.querySelector(".vlabel");
          if(valueLabel){
            valueLabel.parentNode.removeChild(valueLabel);
          }
          
          let valadd  = document.querySelector(".vas");
          if(valadd){
            valadd.parentNode.removeChild(valadd);
          }
          
          let perror  = document.querySelector(".p-error");
          if(perror){
            perror.parentNode.removeChild(perror);
          }
          
          let service_id = i.getAttribute("data-value");
          let service_rate = i.getAttribute("data-rate");
          let service_code = i.getAttribute("data-scode");
          let shipping_type = "";
          let pickup_location = i.querySelector(".sadd").textContent;
          pickup_location_id = i.getAttribute("data-lcode");
          
          formAction(service_type,pickup_location);
          
          let lat = i.getAttribute("data-lat");
          let long = i.getAttribute("data-long");
          
          pan(lat,long);
          
          fetch("/cart.js",{
            method: 'GET',
          })
          .then(function(response){
            return response.json()
          })
          .then(function(data){
            vas(service_id,service_type,s_id,service_code,city_id,district_id,country_id,service_rate,delivery_date,shipping_type,pickup_location);
            cartChange(data,service_id,service_type,s_id,service_code,city_id,district_id,country_id,service_rate,delivery_date,shipping_type,pickup_location,vas_id,vcode)
          })
          .catch(function(err){
            console.log("Error:", err)
          });      
        })
      })
    }
	let map,marker;
    // Initialize and add the map
    function pan(lat,lon) {
      
      let panPoint = new google.maps.LatLng(lat, lon);
      
      map.panTo(panPoint);
      map.setZoom(15);  // This would have to be dynamic, based on data-zoom
    }
    function initMap(data) {
      // The location of store
      const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	  let labelIndex = 0;
      const center = { lat: data[0].latitude, lng: data[0].longitude};
      // The map, centered at store
      map = new google.maps.Map(document.getElementById("map"), {
        zoom: 4,
        center: center,
      });
      // The marker, positioned at store
      
      data.forEach(function(d){
        let lat = d.latitude;
        let lng = d.longitude;
        marker = new google.maps.Marker({
          position:{
            lat: lat,
            lng: lng,
          },
          label: labels[labelIndex++ % labels.length],
          map: map,
        });
        marker.addListener("click",function(){
          map.panTo(new google.maps.LatLng(lat,lng));
        });
      });
    }

    //Service Types

    function servType(data,service_type,s_id,city_id,district_id,country_id){
      let input = "";
      let p = document.createElement("div");
      p.classList.add("service");
      
      let fragment = document.createDocumentFragment();
      
      data.forEach(function(service){
        let d = document.createElement("div");
        d.classList.add("service_type");
        d.setAttribute("data-value",service.id);
        d.setAttribute("data-service",service.name);
        d.setAttribute("data-rate",service.rate);
        d.setAttribute("data-scode",service.service_type_code);
        d.innerHTML=\`<label><input type="radio" name = "stype" value="\${service.id}"><span class="block"> <span>\${service.description}</span>  <span><strong><i>AED\${service.rate}</i></strong></span></span></label>\`;
        fragment.appendChild(d);
      });
      
      p.appendChild(fragment);
      ServiceMethodContainer.appendChild(p);

      let t = document.getElementsByClassName("service_type");
      t = Array.from(t);

      t.forEach(function(i){
        i.addEventListener("change",function(){
          let elem = document.querySelector(".fix");
          if(elem !==null){
            elem.classList.remove("fix");
          }
          i.classList.add("fix");
          
          
          let service_id = i.getAttribute("data-value");
          let service_name= i.getAttribute("data-service");
          let service_rate = i.getAttribute("data-rate");
          let service_code = i.getAttribute("data-scode");
          let shipping_type = service_name;
          let pickup_location = "";
          pickup_location_id = "";
          formAction(service_type,pickup_location);
          
          let dt  = document.querySelector(".datepicker");
          if(dt){
            dt.parentNode.removeChild(dt);
          }
          
          let valueLabel  = document.querySelector(".vlabel");
          if(valueLabel){
            valueLabel.parentNode.removeChild(valueLabel);
          }
          
          let perror  = document.querySelector(".p-error");
          if(perror){
            perror.parentNode.removeChild(perror);
          }
          
          let valadd  = document.querySelector(".vas");
          if(valadd){
            valadd.parentNode.removeChild(valadd);
          }
          
          if(service_name==="SHD"){
            
            let mi = document.createElement("input");
            mi.setAttribute('type', 'text');
            mi.setAttribute('autocomplete', 'off');
            mi.setAttribute('readonly', 'readonly');
            mi.setAttribute('id', 'datePick');
            mi.setAttribute('class', 'datepicker');
            mi.setAttribute('placeholder', 'Choose a date');
            
            ServiceMethodContainer.appendChild(mi);
            
            
            let picker = new Pikaday({
              field: document.getElementById('datePick'),
              firstDay: 1,
              minDate: new Date(),
              maxDate: new Date(new Date(new Date().setDate(new Date().getDate() + 30)).toLocaleDateString("en-US")),
              yearRange: [2000,2020]
            });
            
            document.getElementById('datePick').addEventListener("change",function(){
              let valueLabel  = document.querySelector(".vlabel");
              if(valueLabel){
                valueLabel.parentNode.removeChild(valueLabel);
              }
              
              let valadd  = document.querySelector(".vas");
              if(valadd){
                valadd.parentNode.removeChild(valadd);
              }
              
              let d_date  = document.querySelector(".date-error");
              if(d_date){
                d_date.parentNode.removeChild(d_date);
              }
              
              let mmdate = document.getElementById("datePick").value;
              let delivery_date = new Date(mmdate).toLocaleDateString("en-US");
              
              fetch("/cart.js",{
                method: 'GET',
              })
              .then(function(response){
                return response.json()
              })
              .then(function(data){
                vas(service_id,service_type,s_id,service_code,city_id,district_id,country_id,service_rate,delivery_date,shipping_type,pickup_location);
                cartChange(data,service_id,service_type,s_id,service_code,city_id,district_id,country_id,service_rate,delivery_date,shipping_type,pickup_location,vas_id,vcode)
              })
              .catch(function(err){
                console.log("Error:", err)
              });
            })
          }
          
          else if(service_name==="NDD"){
            let delivery_date = new Date(new Date().setDate(new Date().getDate() + 1)).toLocaleDateString("en-US");
            
            fetch("/cart.js",{
              method: 'GET',
            })
            .then(function(response){
              return response.json()
            })
            .then(function(data){
              vas(service_id,service_type,s_id,service_code,city_id,district_id,country_id,service_rate,delivery_date,shipping_type,pickup_location);
              cartChange(data,service_id,service_type,s_id,service_code,city_id,district_id,country_id,service_rate,delivery_date,shipping_type,pickup_location,vas_id,vcode)
            })
            .catch(function(err){
              console.log("Error:", err)
            }); 
          }
          
          else{
            let delivery_date = new Date().toLocaleDateString("en-US");
            
            fetch("/cart.js",{
              method: 'GET',
            })
            .then(function(response){
              return response.json()
            })
            .then(function(data){
              vas(service_id,service_type,s_id,service_code,city_id,district_id,country_id,service_rate,delivery_date,shipping_type,pickup_location);
              cartChange(data,service_id,service_type,s_id,service_code,city_id,district_id,country_id,service_rate,delivery_date,shipping_type,pickup_location,vas_id,vcode)
            })
            .catch(function(err){
              console.log("Error:", err)
            });
          }
        })
      })
    }
    
    //VAS
    
   	function vas(service_id,service_type,s_id,service_code,city_id,district_id,country_id,service_rate,delivery_date,shipping_type,pickup_location){
      const datax = {shopName:shopName};
      fetch("https://eunimart.minimaltrex.com/eunimart/added_services",{
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(datax)
      })
      .then(function(response){
        return response.json()
      })
      .then(function(data){
        initVas(data.data,service_id,service_type,s_id,service_code,city_id,district_id,country_id,service_rate,delivery_date,shipping_type,pickup_location)
      })
      .catch(function(err){
        console.log("Error:", err)
      });
    }	
  
  	function initVas(data,service_id,service_type,s_id,service_code,city_id,district_id,country_id,service_rate,delivery_date,shipping_type,pickup_location){
      let valueLabel = document.createElement("p");
      valueLabel.classList.add("vlabel");
      valueLabel.innerHTML = "Please select your vas service(Optional):"
      ServiceMethodContainer.appendChild(valueLabel);
      
      let valueservice = document.createElement("div");
      valueservice.classList.add("vas");
      let fragment = document.createDocumentFragment();
      
      data.forEach(function(vas){
        let d = document.createElement("div");
        d.classList.add("vas_type");
        d.setAttribute("data-value",vas.id);
        d.innerHTML=\`<label><input type="checkbox" name="vadd" value="\${vas.name}" data-code="\${vas.vas_code}" ><span class="block">\${vas.name} <span><strong><i>AED\${vas.rate}</i></strong></span></span></label>\`;
        fragment.appendChild(d);
      });
      
      valueservice.appendChild(fragment);
      ServiceMethodContainer.appendChild(valueservice);
      
      let v = document.getElementsByName("vadd");
      let txt=[];
      let vcode= [];
      
      function check(){
        txt=[];
        vcode=[];
        v.forEach(function(i){
          if (i.checked){
            txt.push(i.value);
            vcode.push(i.getAttribute("data-code"));
          }
        })
      }
      
      v.forEach(function(i){
        i.addEventListener("click",function(){
          check();
          fetch("/cart.js",{
            method: 'GET',
          })
          .then(function(response){
            return response.json()
          })
          .then(function(data){
            cartChange(data,service_id,service_type,s_id,service_code,city_id,district_id,country_id,service_rate,delivery_date,shipping_type,pickup_location,txt,vcode)
          })
          .catch(function(err){
            console.log("Error:", err)
          });      
        })
      })
    }

    //Update Cart with Line Item

    function cartChange(data,service_id,service_type,s_id,service_code,city_id,district_id,country_id,service_rate,delivery_date,shipping_type,pickup_location,vas_name,vcode){
      const datax = {quantity:1,line:1,properties:{_eunimart_id: data.token, _random: Math.random()*10}}
      
      fetch("/cart/change.js",{
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(datax)
      })
      .then(function(response){
        return response.json()
      })
      .then(function(data){
        cartUpdate(data,service_id,service_type,s_id,service_code,city_id,district_id,country_id,weight,service_rate,delivery_date,shipping_type,pickup_location,vas_name,vcode)
      })
      .catch(function(err){
        console.log("Error:", err)
      });
    }
    
    //upadte.js

    function cartUpdate(data,service_id,service_type,s_id,service_code,city_id,district_id,country_id,weight,service_rate,delivery_date,shipping_type,pickup_location,vas_name,vcode){
      const datax = {
        attributes:{
          'District-Code': district_id,
          'District-Name': district_name,
          'Checkout-Method': service_type,
          'Delivey-Date': delivery_date,
          'Value-Service': vas_name,
          'Shipping-Type': shipping_type,
          'Pickup-Location': pickup_location,
          'Pickup-Location-Id': pickup_location_id,
          'Vas-Code': vcode,
        }
      }
      
      fetch("/cart/update.js",{
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(datax)
      })
      .then(function(response){
        return response.json()
      })
      .then(function(data){
        saveSession(data,service_id,service_type,s_id,service_code,city_id,district_id,country_id,weight,service_rate,delivery_date,vas_name,vcode)
      })
      .catch(function(err){
        console.log("Error:", err)
      });
    }

    // Updating Session

    function saveSession(data,service_id,service_type,s_id,service_code,city_id,district_id,country_id,weight,service_rate,delivery_date,vas_name,vcode){
      const sessionData = {
        cartToken: data.token,
        country_id: country_id,
        city_id: city_id,
        district_id: district_id,
        weight: weight,
        delivery_date: delivery_date,
        service_type: service_type,
        service_id: service_id,
        id: s_id,
        shopName: shopName,
        shipping_price: service_rate,
        vas_code: vcode,
        shipping_code: service_code,
      }

      fetch("https://eunimart.minimaltrex.com/session",{
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(sessionData)
      })
      .then(function(response){
        return response.json()
      })
      .then(function(data){
        console.log(data)
      })
      .catch(function(err){
        console.log("Error:", err)
      });
    }
    
    //Validation
    
    function euni_valid() {
      let service_district = document.getElementById("city");

      if(service_district.value == ""){
        if(!document.querySelector(".district-error")){
          let dis_error = document.createElement("p");
          dis_error.classList.add("district-error")
          dis_error.classList.add("eunimart-error");
          dis_error.innerHTML = "Please choose a district";
          cityField .appendChild(dis_error);
        }
        //event.preventDefault();
        return false;
      }


      let service_radio = document.getElementsByName("stype");
      let flag=false;
      let service_radio_length  = service_radio.length;

      if(service_radio_length == 0){
        if(!document.querySelector(".p-error")){
          let perror = document.createElement("p");
          perror.classList.add("p-error");
          perror.classList.add("eunimart-error");
          perror.innerHTML = "Please choose an option";
          ServiceMethodContainer.appendChild(perror);
        }
        //event.preventDefault();
        return false;
      }
      else{
        for(let i=0;i<service_radio_length  ;i++) {
          if(service_radio[i].checked) {
            flag=true;
            break;
          }
        }
        if (!flag) {
          if(!document.querySelector(".p-error")){
            let perror = document.createElement("p");
            perror.classList.add("p-error");
            perror.classList.add("eunimart-error");
            perror.innerHTML = "Please choose an option";
            ServiceMethodContainer.appendChild(perror);
          }
          //event.preventDefault();
          return false;
        } 
      }


      let s_date = document.getElementById("datePick");
      if(s_date){
        if(s_date.value == ""){
          if(!document.querySelector(".date-error")){
            let date_error = document.createElement("p");
            date_error.classList.add("date-error");
            date_error.classList.add("eunimart-error");
            date_error.innerHTML = "Please choose a date";
            ServiceMethodContainer.appendChild(date_error);
          }
          //event.preventDefault();
          return false;
        }

      }
    }
    
    document.querySelector("form[action*='cart']").onsubmit = euni_valid;
    
  })
</script>`