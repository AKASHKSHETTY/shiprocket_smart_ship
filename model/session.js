var mongoose = require("mongoose");

var sessionSchema = new mongoose.Schema({
    cartToken: {
        type: String,
        required: true,
    },
   service_type:{
       type:String
   },
   account_id:{
       type:String
   },
   client_token:{
      type:String
   },
   country_id:{
    type:Number
   },
   city_id:{
    type:Number
   },
   district_id:{
    type:Number
   },
   weight:{
    type:Number
   },
   service_id:{
    type:String
   },
   delivery_date:{
    type:Date
   },
   id:{
    type:String
   },
   vas_code:{
       type:Array
   },
   date:{
    type:Object
},shipping_code:{
    type:Number,
},


});

module.exports = mongoose.model('Session', sessionSchema);