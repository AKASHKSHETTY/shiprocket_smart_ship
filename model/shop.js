var mongoose = require("mongoose");

var shopSchema = new mongoose.Schema({
    shopName: {
        type: String,
        required: true
    },
    access_token: {
        type: String,
        required: true,
        unique: true
    },
    install:{ type: String},
    account_id:{ type: String},
    client_token:{ type: String},
    id: { type: String },
    email: { type: String },
    domain: { type: String },
    phone: { type: String },
    country_name: { type: String },
    created_at: { type: String },
    plan_name: { type: String },
    old_access_token: [{ token: String, created_at: {type: Date, default: Date.now} }]
});

module.exports = mongoose.model('Shop', shopSchema);