const express=require('express');
const router=express.Router();
const request = require('request-promise');
const Shop =require('../model/shop');
const Shopify = require('shopify-api-node');
const {
    check_location,
    country_list,
    city_list,
    district_list,
    pick_up_points,
    service_types,
    services,
    addedServices
} = require('../controllers/eunimartController');



router.post('/check_location',check_location);
router.post('/country_list',country_list);
router.post('/district_list',district_list);
router.post('/city_list',city_list);
router.post('/pick_up_points',pick_up_points);
router.post('/service_types',service_types);
router.post('/services',services);
router.post('/added_services',addedServices);



module.exports=router;