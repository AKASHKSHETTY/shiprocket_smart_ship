const router = require('express').Router();
const {
    shopify,
    callback,
    activate
} = require('../controllers/shopifyController');

router.get('/', shopify);

router.get('/callback', callback);



router.get('/activate', activate);

module.exports = router;