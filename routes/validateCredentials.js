const express=require('express');
const router=express.Router();

const {
    validateCredentials,injectSnippet,getShopDetails
} = require('../controllers/validateCredentialsController');


router.post('/',validateCredentials);

router.get('/getShopDetails/:shopName',getShopDetails)

router.get('/:shopName',injectSnippet);

module.exports=router;

