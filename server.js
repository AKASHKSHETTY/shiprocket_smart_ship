const express = require("express");
const app = express();
var path = require("path");
const hbs = require("express-hbs");
const session = require("express-session");
const PORT = process.env.PORT || 8000;
app.set('views', path.join(__dirname, 'views'));
var cron = require('node-cron');
const Session=require('./model/session')
app.set('view engine', 'ejs');
require('dotenv').config();




var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
let DB_URL = process.env.DB_URL;
// const DB = process.env.DB || "order_label_generate";
 mongoose.connect(DB_URL);

var helmet = require("helmet");

app.use((req, res, next) => {
    
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});



var bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

// cron.schedule('* * * * 1 2 3 4 5 6 7 8 9 10 11 12 *', async() => {
//   try {
//   let session=await Session.find();
//   for (var i = 0; i < session.length; i++) {
//     let date1=new Date();
//   let date2=session[i].date;
//   let diff=date2.getTime()-date1.getTime();
//   let days=diff/(1000*60*60*24);
//   if(days>90){
//     await Session.findOneAndDelete({"cartToken":session[i].cartToken})
//   }
//   }
//   } catch (error) {
//     console.log(error)
//   }
//     });
app.set("trust proxy", "loopback");

// app.use(cookieParser());

app.use(
  session({
    key: "user_sid",
    secret: "somerandonstuffs",
    resave: false,
    saveUninitialized: false,
    cookie: {
      expires: 600000 * 100
    }
  })
);



const shopify = require("./routes/shopify");



app.use("/shopify", shopify);

app.use('/validateCredentials',require('./routes/validateCredentials'));

// app.use('/webhooks',require('./routes/webhooks'));

app.use('/eunimart',require('./routes/eunimart'));

app.use('/carrierApi',require('./routes/carrierApi'));

app.use('/session',require('./routes/saveSessionData'));

app.get("/test", function(req, res){
  res.status(200).send({"msg":"App is live"})
});



app.listen(PORT, () => {
  console.log(`Example app listening on port ${PORT}`);
});

